#!/bin/bash
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
# 
# Author: Michael Hammer

DIR=$(dirname "$0")
LOGFILE="${DIR}/update-route53.log"
# Remark: Disabled local storage of last IP - very unreliable - decided to check
# live on the value of the recordset in the route53
# IPFILE="${DIR}/update-route53.ip"

if [ ! -e "${DIR}/update-route53.conf" ]; then
    echo "We need a config file '${DIR}/update-route53.conf'"
    exit -1;
fi

source "${DIR}/update-route53.conf"

function logit()
{
    echo "$*"
    echo "$(date '+%b %d %H:%M:%S'): $*" >> ${LOGFILE}
}

function valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
	OIFS=$IFS
	IFS='.'
	ip=($ip)
	IFS=$OIFS
	[[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
		 && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
	stat=$?
    fi
    return $stat
}

# Check if the IP file exists
# if [ ! -f "$IPFILE" ]
# then
#     touch "$IPFILE"
#     chown ${AWS_USER}:${AWS_GROUP} "$IPFILE"
# fi

# Check if the log file exists
if [ ! -f "$LOGFILE" ]
then
    touch "$LOGFILE"
    chown ${AWS_USER}:${AWS_GROUP} "$LOGFILE"
fi

# Get the external IP address from OpenDNS (more reliable than other providers)
MY_IP=`dig +short myip.opendns.com @resolver1.opendns.com`
AWS_IP=`aws route53 list-resource-record-sets \
    --hosted-zone-id Z1UBLTHJCXG9BB \
    --profile ${AWS_PROFILE} | \
    jq -r --arg RECORDSET "${RECORDSET}." \
    '.ResourceRecordSets | .[] | select(.Name==$RECORDSET) | .ResourceRecords | .[] | select(has("Value")) | .Value'`

if ! valid_ip $MY_IP; then
    logit "Invalid IP address: $MYIP"
    exit 1
fi

# Check if the IP has changed
if [[ "$MY_IP" == "$AWS_IP" ]]; then
    # code if found
    logit "IP on AWS ($AWS_IP) is equal to current IP ($MY_IP). Exiting ..."
    exit 0
else
    logit "IP has changed from $AWS_IP to $MY_IP"
    # Fill a temp file with valid JSON
    TMPFILE=$(mktemp /tmp/temporary-file.XXXXXXXX)
        cat > ${TMPFILE} << EOF
	{
	    "Comment":"$COMMENT",
	    "Changes":[
		{
		    "Action":"UPSERT",
		    "ResourceRecordSet":{
			"ResourceRecords":[
			    {
				"Value":"$MY_IP"
			    }
			],
			"Name":"$RECORDSET",
			"Type":"$TYPE",
			"TTL":$TTL
		    }
		}
	    ]
	}
EOF
    #chown ${AWS_USER}:${AWS_GROUP} ${TMPFILE}
   	
    # Update the Hosted Zone record
    #su ${AWS_USER} -c "
    logit `aws route53 change-resource-record-sets \
        --profile ${AWS_PROFILE} \
        --hosted-zone-id $ZONEID \
        --change-batch file://${TMPFILE}`
    # Clean up
    # echo "${TMPFILE}"
    rm ${TMPFILE}
fi

# All Done - cache the IP address for next time
# echo "$MY_IP" > "$IPFILE"
